package modelo;
public class Docente {
    private int numDocente;
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pago;
    private int horasImpartidas;
    
    Docente(){
    }
    
    Docente(int numDocente,String nombre,String domicilio,int nivel,float pago,int horasImpartidas){
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pago = pago;
        this.horasImpartidas = horasImpartidas;
    }
    
    Docente(Docente D){
        numDocente = D.numDocente;
        nombre = D.nombre;
        domicilio = D.domicilio;
        nivel = D.nivel;
        pago = D.pago;
        horasImpartidas = D.horasImpartidas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPago() {
        return pago;
    }

    public void setPago(float pago) {
        this.pago = pago;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPago(){
       float pagoBase = 0.0f;
       switch(nivel){
            case 1:
                pagoBase = 30.0f;
                break;
            case 2:
                pagoBase = 50.0f;
                break;
            case 3:
                pagoBase = 100.0f;
                break;
            default:
                System.out.println("Nivel inválido");
       }
       return pagoBase*horasImpartidas;
    }
    public float calcularImpuesto() {
        return calcularPago() * 0.16f;
    }
    
    public float calcularBono(int cantidadHijos) {
        float bono = 0.0f;

        if (cantidadHijos >= 1 && cantidadHijos <= 2) {
            bono = calcularPago() * 0.05f; 
        } else if (cantidadHijos >= 3 && cantidadHijos <= 5) {
            bono = calcularPago() * 0.10f; 
        } else if (cantidadHijos > 5) {
            bono = calcularPago() * 0.20f; 
        }
        return bono;
    }
}





